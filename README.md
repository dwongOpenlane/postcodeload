This is a nodejs app to read a post code file from S3 and insert into our ES.

ES is indexed by country and the post code with autocomplete.

The S3 file name will have the country so we know which country the post codes are being loadd for.

This app deletes and reloads post codes now and will need to be updated when we know exactly how post code refresh/reload and for which countries occur.